import Adafruit_DHT as dht

class CIoTSDHT11():
    
    def __init__(self):
        self.dht = dht
        self.dht_sensor = dht.DHT11
        self.dht_pin = 17

    # read the sensor values
    def read_sensor(self):
        humidity, temperature = self.dht.read(self.dht_sensor, self.dht_pin)
        if humidity is not None and temperature is not None:
            return {"temperature":temperature, "humidity":humidity}
        else:
            return None