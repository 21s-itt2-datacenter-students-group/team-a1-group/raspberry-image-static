#!/usr/bin/python
from time import sleep
from ciots_attributes import CIoTSAttributes

class CIoTSPublisher(CIoTSAttributes):

    def __init__(self, sensor_name, client_name):
        super().__init__(sensor_name, client_name)


if __name__ == '__main__':

    try:
        publisher1 = CIoTSPublisher("sensor_1", "DHT11_0")

        publisher1.client.on_connect = publisher1.on_connect
        publisher1.client.on_disconnect = publisher1.on_disconnect

        publisher1.connect_to_broker()
        #publisher1.client.loop_start()

        while True:
            data = publisher1.read_sensor()
            if data != None:    
                payload = publisher1.build_payload(data, publisher1.sensor_name)
                print(f'Publishing {payload} to topic, {publisher1.topic}')
                publisher1.client.publish(publisher1.topic, payload)        
                sleep(publisher1.publish_interval) # wait x seconds

    except Exception as e:
        print(e)
        #publisher1.client.loop_stop() #stop the loop
        #publisher1.client.disconnect()
        exit(0)



