from sys import path as syspath
from os import path as ospath
from socket import error as socket_error
from json import loads, dumps as jsondumps, JSONDecodeError
from time import time


class CIoTSTools:

    def connect_to_broker(self):
        try: 
            print("connecting to broker")
            self.client.connect(self.mqtt_broker_addr, self.mqtt_broker_port)
            self.client.will_set("ciots/status", payload=self.client_name + " went offline unexpectedly!", qos=0, retain=True)
        except socket_error as e:
            self.client.loop_stop()
            print(f"could not connect {self.client_name} to {self.mqtt_broker_addr} on port {self.mqtt_broker_port}\n {e}")
            exit(0)


    def on_connect(self, client, userdata, flags, rc):
        print(f'connected to {self.mqtt_broker_addr} on port {self.mqtt_broker_port} ')
        client.publish(self.status_topic,payload=self.client_name + " is now online!")


    def on_disconnect(self, client, userdata, rc):
        if rc != 0:
            print("Unexpected disconnection!")
            self.blink_red_led()



    def on_message(self, client, userdata, message):
        try:
            document = loads(message.payload) # create a dictionary from MQTT received message
            print(f'attempting db store: {document}') # print the document for development purposes
            return_id = self.db_collection.insert_one(document) # insert the document in the database
            print(f'stored in db with _id: {return_id.inserted_id}') # print the returned document _id from the database
        except JSONDecodeError:
            print("Error: message was not formatted as JSON")


    def on_subscribe(self, client, userdata, mid, granted_qos):
        print(f"Succesfully subscribed to f{self.topic}")


    # create payload
    def build_payload(self, sensor_readings, sensor_id):
        timestamp = int(time() * 1000)
        payload = {'sensor_id': self.sensor_name, 'timestamp':timestamp}
        for key, value in sensor_readings.items():
            payload[key] = value
        return jsondumps(payload) # convert to json format

    
    # open and strip token
    def get_token(self, filename):
        with open(ospath.join(syspath[0], filename), 'r') as f:
            token = f.readline()
        return token.strip()

    

    def blink_red_led(self):
        self.red_led.blink(0.5,0.5,5)
        