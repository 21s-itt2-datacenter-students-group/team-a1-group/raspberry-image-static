import paho.mqtt.client as mqtt
from ciots_tools import CIoTSTools
from ciots_dht11 import CIoTSDHT11
from ciots_fcs01 import CIoTSFCS01

class CIoTSAttributes(CIoTSTools, CIoTSDHT11, CIoTSFCS01):

    ## If sensor_name is filled out, publisher config is chosen or set as blank for subscriber config
    def __init__(self, client_name = "N/A", sensor_name = "N/A"):
        self.client_name = client_name
        self.client = mqtt.Client(self.client_name) #init client
        self.mqtt_broker_addr = 'broker.hivemq.com'
        self.mqtt_broker_port = 1883
        self.status_topic = "ciots/status"
        
        if sensor_name == "N/A": ## Subscriber
            from pymongo import MongoClient
            self.topic = "ciots/sensors/#"
            self.mqtt_to_db()
        else: ## Publisher            
            from gpiozero import LED
            self.sensor_name = sensor_name
            self.topic = "ciots/sensors/" + self.client_name
            self.publish_interval = 2
            self.red_led = LED(26)
            self.green_led = LED(19)

            if "dht11" in self.sensor_name.lower():
                CIoTSDHT11.__init__(self)
            elif "fcs01" in self.sensor_name.lower():
                CIoTSFCS01.__init__(self)


    def mqtt_to_db(self):
        self.token = self.get_token('token')
        self.dbUser = "dbAdmin"

        self.db_client = MongoClient(f"mongodb+srv://{self.dbUser}:{self.token}@cluster0.xxw3w.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
        self.db = self.db_client.mqtt_test
        self.db_collection = self.db.dht11
    
    
